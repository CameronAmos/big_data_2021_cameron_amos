# Welcome to BIOL890x: Data science for biologists

Spring odd years

Meeting times:
Tues, Thur: 2:30-3:45 via Zoom (https://ksu.zoom.us/j/93426990844?pwd=VGR2VlZIQmNCRlMwTTFTdXhlVmVCZz09)
Wed: 2:30-3:20 via Zoom (https://ksu.zoom.us/j/93426990844?pwd=VGR2VlZIQmNCRlMwTTFTdXhlVmVCZz09)

The content of the course is available on Bitbucket: ()

**Books and Resources**

"Hands-on Machine Learning with Scikit-Learn, Keras, and TensorFlow�, 2nd Edition

To access for free, follow this link: https://guides.lib.k-state.edu/c.php?g=181701&p=1196666, link to "Safari Online". Register for an account and add this to your library.

"Quantitative Economics with Python"

https://lectures.quantecon.org/py/



Enrollment limit: 10

Credits: 4

**Description:** This is an advanced graduate course aimed at biologists interested is leveraging advanced analytical tools to understand complex data sets. Data sets can be genomics, images, ecological data, literature meta data, or pretty much any data set you can think of with a problem that requires data analysis to make inference. This course will focus on leveraging Python3 and its various analytical and machine learning packages to be able to analyze complex data sets. This class is applied, where we will cover the theory in enough detail to understand how an analysis works, but focus primarily on application of these concepts to actual biological data. For students interested in building a theoretical background, you are encourage to consider the statistics certificate program that will augment this class with a mathematical view of the theory behind the concepts in this course.

The skills obtained in this course will be broadly applicable to academic problems. The skills learned in this class will also be a spring board for those interested in industrial jobs. Both academic and industrial science increasingly requires both "wet lab" and computational analytical skills. This course will bridge that gap for biologists.

**Format:** This class is hands on laboratory where you will get your hands dirty with data. We will work interactively with data and code in class. What does this mean? This course is not a "lecture" course. Instead, we will work together on learning concepts by writing code together. Students will work in peer teams to solve problems together. A typical class will start with a brief overview of a new concept. This will then be followed by working in a group format to address example problems using a new concept. Students will make extensive use of best practices for coding, peer review of code and good software engineering practices. While the syllabus may seem intimidating to a biologist that is new to data science, the purpose of this course is to jump start your ability to develop results from large and complicated data sets.

In addition, students and the instructor will collaboratively create notes for each class using the Etherpad platform. Similarly, students will submit their coding from class to their git repository at the end of each class, after which a round-robin will be used to peer review each other's code. There are no exams in the course, all grading is based on participation. In addition to coding, students will master the material by peer reviewing the code of their peers. Communication via git and comments will help train students in best practices for developing software in team environment. In addition to being the best practices for software in general, the principles learned in class will prepare students for data science careers in industry.

**Prerequisites:**

1) Working knowledge of the command line interface and Python3. We will start coding with Python3 in the second week of the course. For those who have not programmed in Python previously, there are a few options for getting up to speed.

A) Udemy course on Python (self paced) https://www.udemy.com/complete-python-bootcamp/

This is a self paced online course that introduces the fundamentals of Python3 and Jupyter Notebooks to students. Best of all, you can enroll in this course for about $10-20. Dr. Olson will be running group to take this course together starting October X 2018 in preparation for the Data Science course. Dr. Olson will also introduce students to the Unix command line interface. If you are a biologist and computational biology is something you want to learn, the bootcamp and the 890 course will get you into data science quickly.

B) Prior knowledge of Python3. Students will need to provide code samples to demonstrate prior knowledge of Python3

C) CIS "no credit python course". CIS offers a no credit Python course. Details TBD.

2) A working knowledge of git, and version control

3) A desire to learn modern analytical methods for analyzing complex data sets.

**Grading**

This class relies solely on weekly and daily projects and peer review of code. 40% of your grade is based on the instructor's analysis of your code for weekly and daily projects, 40% of your grade is based on peer review of your code. 20% of your grade is based on participating in effective peer review of others' code. For example, on a weekly project the instructor will review your code, as well as a peer group. Both will assign a grade for the assignment. At the same time, you will peer review another groups code and receive participation points based preparing an effective code review.

***Grading scale***

90-100%: A

80-89.9: B

70-79.9: C

60-69.9: D

<59.9: F


**How homework is handled in BIOL890**

In this class, we will use Git and our class repository to submit and peer review homework.

## Week 1: Toolchain bootcamp

**Wednesday:** Git tutorial. Installation of a common Python3 and Jupyter environment with Anaconda. Understanding environment management in Anaconda and Bioconda.

Today's Collaborative Note: https://hackmd.io/@wfmcHSIITfq31Vz2PLtHWw/ByJMSryed/edit

 Lightning review of the command line interface.
Code challenge: bulk file manipulation with bash shell scripts

Today's Collaborative Note:

**Thursday:** Git, and intro to BeoCat

Git tutorial: https://www.atlassian.com/git/tutorials

Today's Collaborative Note: https://hackmd.io/c9g7IKfNSOCH_UDO6i1Psw


## Week 2: Python Bootcamp

**Weekly Project:** Toolchain and Python up to speed check. Peer

*Daily Juypter notebooks due 2/08/2021 pushed to Bitbucket*
*Peer review due 2/10/2021 at 5p pushed to Bitbucket*



**Tuesday:** Python3 Bootcamp and Lightning review of Python3

https://hackmd.io/@wfmcHSIITfq31Vz2PLtHWw/B1kkpEvld/edit


**Wednesday:** Python3 Bootcamp and Lightning review of Python3 continued

https://hackmd.io/@wfmcHSIITfq31Vz2PLtHWw/B1qkDKuxd/edit

**Thursday** Python3 Bootcamp and Lightning review of Python3 continued




## Start learning Pandas ##

O'reilly/Packt

"Data Analysis with Pandas and Python" by Boris Paskhaver

https://learning.oreilly.com/videos/data-analysis-with/9781788622394/

Python for Data analysis

https://learning.oreilly.com/library/view/python-for-data/9781491957653/

Pandas for Everyone

https://learning.oreilly.com/library/view/pandas-for-everyone/9780134547046/


## Week 3: Reproducible research

**Weekly Project:**

*Due: Sunday 2/10/2019 at 5p pushed to Bitbucket*
*Peer review due 2/12/2019 at 2p pushed to Bitbucket*

**Tuesday:** Structuring a project for success. Software engineering, unit tests, maintainability

https://hackmd.io/vhFYKFEoT4SRREBgh4cplQ

*Reading https://journals.plos.org/plosbiology/article?id=10.1371/journal.pbio.1002303 *


**Wednesday:** NextFlow (https://www.nextflow.io)

https://www.youtube.com/watch?v=8_i8Tn335X0

https://www.youtube.com/watch?v=j5Xc8mUmDMc

https://www.youtube.com/watch?v=xmNUtboTThA

https://www.youtube.com/watch?v=XFrVtD-RPzY

https://www.youtube.com/watch?v=IcMz6JE8n_M

https://seqera.io/training/


https://github.com/seqeralabs/nextflow-tutorial


**Thursday:** NextFlow 2 (https://www.nextflow.io)

* Due: 2/15/2021 at 5p pushed to BitBucket
* Peer review due 2/17/2021 by 5p pushed to BitBucket

Today, we will build a NextFlow pipeline

https://hackmd.io/@wfmcHSIITfq31Vz2PLtHWw/rJQW-f7Zd/edit


## Week 4: Obtaining data with Pandas and exploration. Numerical analysis with Numpy

**Weekly Readings:**

Advanced Python Programming: https://lectures.quantecon.org/py/index_advanced_python_programming.html

Data and Empirics: https://lectures.quantecon.org/py/index_data_and_empirics.html

Hands On Machine Learning Chapter 2, through pandas.

*If you want an advanced tutorial on Pandas, this O'reilly video series is excellent, but NOT requires: https://learning.oreilly.com/videos/data-analysis-and/9781789343205*

**Weekly Project:** Explorative analysis of Konza rain and irrigation versus productivity

**Tuesday:** Obtaining information in data frames using Pandas, Data frame manipulation and visualization for data exploration

https://hackmd.io/@wfmcHSIITfq31Vz2PLtHWw/SyPPEkQzu/edit

**Wednesday:** Continued from Tuesday

**Thursday:** Numerical analysis with Numpy in Python3



# UPCOMING #



## Week 5: Matrix theory bootcamp

**Weekly Project:** A break from Python to do some math problems - all coding this week will be with numpy

**Weekly Readings and Videos**

3Blue1Brown series on linear algebra:

https://www.youtube.com/watch?v=fNk_zzaMoSs&list=PLZHQObOWTQDPD3MizzM2xVFitgF8hE_ab

"Basics of Linear Algebra for Machine Learning PDF"

Sections I and II for Tuesday, Section III and IV for Thursday

**For those that really want to build their data science skills, I strongly recommend this video course***

https://learning.oreilly.com/videos/math-for-machine/


**Tuesday:** A quick tour of mathematical concepts

**Wednesday:** Elementary linear algebra I

**Thursday:** Elementary linear algebra II



## Week 6: Data visualization with Matplotlib, Seaborn, Choroplets (geographical/spatial)

**Weekly Readings:**

Hands on Machine Learning, Chapter 2, section "Discover and Visualize the Data to Gain Insights"

The scientific libraries: https://lectures.quantecon.org/py/index_python_scientific_libraries.html

Pandas again: https://lectures.quantecon.org/py/index_data_and_empirics.html



**Weekly Project:** Analysis of Konza LTER data graphically and/or geographically

**Tuesday:** Integrating Pandas and Numpy with Matplotlib

* Tutorial for class today and Wednesday: https://www.datacamp.com/community/tutorials/matplotlib-tutorial-python *

**Wednesday:** Matplotlib II

Today we will break in pairs and find a scientific question to address with Konza LTER data. Each group will have a different data set/question.

**Thursday:** Seaborn to plot statistical data

* Tutorial for class today https://elitedatascience.com/python-seaborn-tutorial *




# Upcoming #

## Week 5: Introduction to Machine Learning and scikit
**Weekly Project:** Machine learning on the "iris data set"

**Tuesday:** Introduction to Machine Learning and pitfalls

**Wednesday:** Getting started with scikit

**Thursday:** Scikit II with example data


## From week 6 and beyond, specific lessons may be adjusted to match student needs and data sets

## Week 6: Linear and Logistic regression
**Weekly Project:** Predicting plant height at Konza based on rainfall patterns

**Tuesday:** Linear regression

**Wednesday:** Logistic regression

**Thursday:** Model training

## Week 7: K-nearest neighbors

**Weekly Project:** Classification of gene expression patterns in a developmental series

**Tuesday:** K-nearest neighbors concepts working through sample data

**Wednesday:** Tuning K-values to improve predictions

**Thursday:** Project group work

## Week 8: Decision trees and random forests
**Weekly Project:** Credit worthiness prediction

**Tuesday:** Decision trees with examples

**Wednesday:** Random forrest analysis

**Thursday:** Compare a decision tree preduction to a random forrest prediction

## Week 9: Principal component and Multidimensional scaling
**Weekly Project:** Grouping of RNAseq data

**Tuesday:** PCA and MDS concepts with code

**Wednesday:** PCA and MDS stepwise in Python

**Thursday:** PCA and MDS in scikit

## Week 10: K-means clustering
**Weekly Project:** K-means clustering of RNAseq data to find co-expressed genes

**Tuesday:** K-means clustering - coding from the ground up

**Wednesday:** K-means clustering in Seaborn

**Thursday:** Advanced K-means clustering and project work

## Week 12 & 13: Neural nets and deep learning
**Project:** Feature recognition in image data

**Tuesday:** Neural network theory

**Wednesday:** TensorFlow - concepts and eager execution

**Thursday:** TensorFlow - eager exectution with trained models

**Tuesday:** Model training, gradient descent and optimization I

**Wednesday:** Model training, gradient descent and optimization II and Project work

**Thursday:** Project work
