#!/usr/bin/env nextflow

//This python process lists every .fastq file in an input directory and 
//saves the output to that fastq file .sample file.

process sample_fastqs {

	output:
	file 'thefastq.sample'
	
	shell:
	'''
	# Grab each fastq file from the folder provided and output the first and last 10 lines into thefastq.sample
	for f in /homes/camos333/dataclass/big_data_2021_cameron_amos/NextFlowBasicProject/fastq_folder/*.fastq; do
		echo "Here are the first 10 lines:" > thefastq.sample
		echo "$f" | head -n 10 "$f" >> thefastq.sample
		echo "Here are the last 10 lines:" >> thefastq.sample
		echo "$f" | tail -n 10 "$f" >> thefastq.sample
	done
	'''

}             
process pyTaskRegex {

	input:
	file 'thefastq.sample'
	
	output:
	file 'thefastq.log'
	'''
	#!/usr/bin/env python
	import glob, os, sys, re
	# This function grabs each fastq file from the fastq_folder and adds its name to a list which is passed onto find_how_many_reads
	def find_files():
		fastq_file_list = []
		input_directory = "/homes/camos333/dataclass/big_data_2021_cameron_amos/NextFlowBasicProject/fastq_folder"
		os.chdir(input_directory)
		for file in glob.glob("*.fastq"):
			fastq_file_list.append(file)
		return fastq_file_list
	fastq_file_list_out = find_files()
	
	# This function finds the @ signs at the beginning of each read entry, counts how many there are, and then checks the sequence underneath to see if there is an 'N' character and counts it.
	def find_how_many_reads(file_list):      
		f = open("thefastq.log", "w")
		pattern_read = re.compile(r'^@')
		pattern_N = re.compile(r'N')
		
		# Initialize counts and nucleotide_start_line_list, which keeps track of what line each read entry starts at so it can look at the line after.
		total_read_count = 0
		total_reads_with_N = 0
		nucleotide_start_line_list = []
		#Iterate through all files in the file list
		for file_in_directory in file_list:
			#Iterate through each line in each file after opening it.
			for line_number, line in enumerate(open(file_in_directory)):

    
				# If while iterating it is currently on the line after the start of a read, check for Ns in the sequence and report it to a log file.
				if line_number-1 in (nucleotide_start_line_list):
					for match in re.finditer(pattern_N, line):
						total_reads_with_N = total_reads_with_N + 1
						print ("Found a read with an 'N' character on line "+ str(line_number))
						f.write("Found a read with an 'N' character on line "+ str(line_number))
				# Otherwise, it is a read entry title. Count the read and output it to a log file.
				else:
					for match in re.finditer(pattern_read, line):
						total_read_count = total_read_count + 1
						print ("Found a read on line " + str(line_number+1))
						f.write("Found a read on line " + str(line_number+1))
						nucleotide_start_line_list.append(line_number+1)

		#Print total reads and total reads with N characters by using the counts.
		print ("Total reads: " + total_read_count)
		f.write("Total reads: " + total_read_count)
		print ("Total Reads with at least one wildcard 'N' character: " + total_reads_with_N)
		f.write("Total Reads with at least one wildcard 'N' character: " + total_reads_with_N)
		f.close()
	find_how_many_reads(fastq_file_list_out)
		'''
}

